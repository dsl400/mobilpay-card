# MobilPay Card
​

![alt text](https://saltandpepper.co/wp-content/uploads/2020/04/NPMjs-cover-mobilPay.jpg "Cover image")

### About
This is a  very lightweight NodeJS library to integrate [Netopia mobilPay](https://www.mobilpay.ro/public/) payment gateway in your projects.
- builds mobilPay request from the input data
- adds split payments field to the request to send part of the payment to another account
- validates mobilPay response based on the private key associated with the account
​
### Installation
```sh
npm install mobilpay-card
```
​
### Usage
The first step for using this library is to extract the public key from the public certificate given to you by Mobilpay
```sh
openssl x509 -pubkey -noout -in public.cer > public.pem
```
​
You can choose to use the contents of the `.pem` or `.key` files, or the files themselves.
​
Import the library
```javascript
const MobilPay = require('mobilpay-card') // ES5
import MobilPay from 'mobilpay-card' // ES6
```


| `constructor` params | Type | Description |
| ------ | ------ | ------ |
| signature | `String` | Signature provided by Mobilpay |
​
```javascript
const mobilPay = new MobilPay(
    'XXXX-XXXX-XXXX-XXXX-XXXX', // Mobilpay signature
);
```
​
After initialization, you need to set up the public key
​
You can either do it from file, or from string:
```js
mobilPay.setPublicKey('-----BEGIN PUBLIC KEY-----\n' +
        'MIGfMA0GCSqGSIb3DQEBAQUAA4GNiQKBgQC8IdzYRKWRbir4IWfTe+Ql22t\n' +
        'OTFjQoeNtpHHxSm6j+WFYglAYNzHOWWHdXtF4vVItUCNmfaw2RkMI2qwKa9\n' +
        '0vW6MBxJGR/NWDxwWW2KQNvASMh2EXGk147YgRr46cLs5Y5l3gaFS4pyGhN\n' +
        'CFKTHp/TC1htnxjHXQIDAQAB\n' +
        '-----END PUBLIC KEY-----\n');
```
or
```js
setPublicKeyFromPath ('<path_to_pem_file>') 
```
​
Initialize and set the client and payment details

| `setClientBillingData` params | Type | Description |
| ------ | ------ | ------ |
| firstName | `String` | The client's first name |
| lastName | `String` | The client's last name |
| county | `String` | The client's county |
| city | `String` | The client's city |
| address | `String` | The client's address |
| city | `String` | The client's city |
| email | `String` | The client's email |
| phone | `String` | The client's phone number |

| `setPaymentData` params | Type | Description |
| ------ | ------ | ------ |
| orderId | `String` | The unique identifier for the payment |
| amount | `Number` | The amount to be paid |
| currency | `String` | The currency in which the payment will take place |
| details | `String` | The details of the payment |
| confirmUrl | `String` | The url which the MobilPay API should call for confirmation |
| returnUrl | `String` | The url which the MobilPay API should return after confirmation |
​
```javascript
mobilPay.setClientBillingData({
    firstName: 'tes',
    lastName: 'ting',
    county: 'test',
    city: 'test',
    address: 'test',
    email: 'tes@ting.com',
    phone: '0799999999',
});
mobilPay.setPaymentData({
    orderId: Date.now().toString(),
    amount: 1,
    currency: 'RON',
    details: 'No details',
    confirmUrl: 'http://confirm.url',
    returnUrl: 'http://return.url',
});
```
You can also set a different shipping address by using `mobilPay.setClientShippingData({...})` which has the same parameters as `mobilPay.setClientBillingData({...})` presented above.
​
For setting up split payments you can use the `setSplitPayment` function:

| `setSplitPayment` params | Type | Description |
| ------ | ------ | ------ |
| destinationId | `String` | The destination sac_id for the master |
| destinationAmount | `Number` | The amount to be paid to the master |
​
```javascript
mobilPay.setSplitPayment('<sac_id>', 1);
```
​
To build the request

| `buildRequest` params | Type | Description |
| ------ | ------ | ------ |
| sandbox | `Bool` | Use for sandbox |
​
```javascript
let request = mobilPay.buildRequest(true);
```
​
The `request` that will be constructed will look like:
​
```json
{ 
    "url": "http://sandboxsecure.mobilpay.ro",
    "env_key": "OQR4VUMOHY1W+jMcE8NCc7Es2mf37+lqECwygW8rS1O55E2kkwwZqY9oyG4WuXeyN7rjIiC3YvmvJ1od8+5f2p1ygxe4H1gp0naxfEi52W/PAuoChgqkVKswvI67kzKg3yc7JGpbPcOp+hTgnTAzegWGb69WTpLxWf+HGHs0A/o=",
    "data": "OImfydaqxpWVm0ygudhUNaV0vlBkpHSEcs+gqX5z8vr1rt5KXCLtODtTsXo2/B4D7sYpIj21sm4hr4M/80/DuaH/46tw2v602PUs3MdVqY7/KWiDUw3EywwB5Vjvwqd4DfIUtOBYTvbTPvnj2Ly5MdUWxtJpOFkE3UbmmbJqDYIqQDus/G8EisHeJI73pWumoSOZjYpukV6wj4uh9Pbp/GlnaFPIvKWECF0lBx8yr6kYbLGcDYqB6ly8yAtr1BOcCmcfV2J5BGbnPnF3RbAdGYvwd5Vt2MvzdETeoYrtb+hTjw1c2HSo/P/NhbKzk9IOA6ZkMcDE4Lti82c4QkwfwSTtyYlrkszk1CmU+m3r6if0qPqvTd5KTn7Zi/YcRNvwYmu05MKgth8MuAl/guqx37H9o6dSBDpBmCi+fRQ09J/BvzjIihAVpqypayByRu/jB6KJ4PZrBMHdsTdkbjDosuWfJPeEl+HwkWXnTsTdemGjsxpIE80Cat8c2ma/LHMvcRkpowOwX6YAhKBifI+s7zKbbA9L351YqgUSwKLXT2IuWiaPWIF+0ppSMI3Kc3Eqd6GdnFLy1Ku+IX3wpY4rOetbdYdotdLSfTceThx1raQfA6UMqbW7JdRgo++SfLXXO+p9QatWps+ZvLjhFCqufF5l0SeNFyXvmVkk1Qvu0uBxLy+V1t6qoX3SiVho/tEIFf6EcZ3Rp8pHUo+YTQmME+kXP0lexp6Ur3BNYiCqmi6Vobg23IguNzjZV0PIIH0etbq186Rqcz9otmPekO6/z1cZKTVtgoO2t+5PNWQ4ivjvSizUlV2Ltv3D7YqC1bOMJR4rrKarMLUnczAoWplT9OIK7eSnwo3kF7/vceKZJt4J+CbRBWHGATV/c7ktgkJXoOyPyjq8NqRnkX2ECnRVwegOa/ZIoIldPUuQoEqi1Ie5m4IerUDULBVoOGzbzEeq+3H2oGTPyoyCdmK1nz6DiDTMrnRb7C++hPz2+MN6DxYYFAIkXmh6NzfLTK8x/vVeSHkCea1drjSIUTscx9U+uYtcgUpl81IIhGGbvoCaqScf7Pedrj8pZujyX34DBqJ0wdHtFwu3jDL5znRiCIqlmJCYErweJoUCcTphDvpUwY6vWOise+5n33gCf1/FrUrXRApcU9N3/HokiT90cIfyK95TGunU5Q=="
}
```
To send a request to Mobilpay, you need to setup a `form` with `method = POST`, and `action = request.url`.
As inputs for the form, you need to send the `request.env_key` and `request.data`.
​
After being processed and the payment has been made, the Mobilpay API will make an API call to the `confirmUrl` set above. The `confirmUrl` should be an endpoint on the API you are building, because it needs to verify the response from Mobilpay.
​
To verify, you need the `validatePayment` method which take the `env_key` and `data` from the Mobilpay request body, and also set the private key (from file or string)

| `validatePayment` params | Type | Description |
| ------ | ------ | ------ |
| env_key | `String` | The env_key from the Mobilpay request body |
| data | `String` | The data from the Mobilpay request body |
​
```js
const {env_key, data} = body
mobilPay.setPrivateKey('-----BEGIN PRIVATE KEY-----\n' +
        'MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBARG+VH3kcWa\n' +
        'cheCdCB/EwUZYFELepVGldTsDIt/w7h9Bi/55+Eq0HjBp9zqjZh67akEQKb\n' +
        'x1ilA87XkrBKXTvGzyszglz6UbfLhuLg1UfmjJst9cOtwPOANewKHv2uJio\n' +
        'wqqolt+OImKm0MO0/+MM/z8n4szPAgMBAAECgYEA8JL6OkIBO+Iy7BvyUe6\n' +
        'g0ySK9drjclUFwYUZLwUMzmOToQ4yVECsKYZMbwq4jXRmcMo9mwQxOt3Zvc\n' +
        'ukwcwbnhDbUY2pgEr+SMasYzEErg+pJLhLkWCs8tJL+YppV3T9LelekBwY3\n' +
        'bQmWdbaLv56P+5w7QIECQQD7SmicemdHGwmhEz13nbOynmP0hyFYYkKmUSn\n' +
        'R6VpunCD9G3thIBJfFVyg4EDHqOQIMekypTcd8XRAmHJAkE4Hia8EXJA6hf\n' +
        'ATkaasI6R79ZriOUp7W2jqSGQ1UtujY3n7TuNuE0GjISgYwbhcowabJKEVJ\n' +
        '5gvF1wJAVjYM9cI4tHheMVi8edEs2Vbly/rJmM+U5N21emFiumvuFWfcSFI\n' +
        'Me3qEsNy+3MDgmr8k1i9AXZF85LxoQJBALRifaFlWVgu++lHc+sg5t6xJJx\n' +
        '1qIm2rc1jH2WAAdRNeczxjOwA8Etj3s+FjRMgmDjEuGWBzyjuECQQCj/DtM\n' +
        '+b7wtPqMtet6cbf8Mc45vJnvmIpviG/BMYi8dlQFty1gzw/dNM47umAVxTR\n' +
        '9JSX2ToP3Qt102qK\n' +
        '-----END PRIVATE KEY-----');
​
// or
// setPrivateKeyFromPath ('<path_to_private_key_file>')
​
let response = mobilPay.validatePayment(env_key, data)
​
if (response.error) {
    /* 
    * Code in case of error
    */
    res.set(response.res.set.key, response.res.set.value)
    res.status(200).send(response.res.send)
    return
}
/*
* Code in case of success
*/
switch (response.action) {
    case 'confirmed': 
        // do something
        break;
    case 'paid': 
        // do something
        break;
    case 'paid_pending':
        // do something
        break;
    case 'confirmed_pending':
        // do something
        break;
}
res.set(response.res.set.key, response.res.set.value)
res.status(200).send(response.res.send)
return
```
A successful `response` from the validation looks like this
```json
{
    "action": mobilpayAction,
    "errorMessage": null,
    "error": null,
    $: {...},
    "orderInvoice": {...},
    "res": {
        "set": {
            "key": "Content-Type",
            "value": "application/xml"
        },
        "send": `<?xml version="1.0" encoding="utf-8" ?><crc>errorMessage</crc>`
    }
}
```
​
The `returnUrl` can be a static page to indicate the end-user that the payment has been made.
​
### Todo
* Automatic public key extraction

### About the [Salt&Pepper](https://saltandpepper.co/careers/) team
```sh
We are a full-service software company dedicated to building digital products that matter.
```
We do this by providing a complete range of services required for building great products: consulting services, business analysis, UI/UX design, web/mobile development, quality assurance and maintenance.

We consider ourselves to be a new generation company, a mix between the world of the big agencies and the startup ecosystem. We can describe us as a multi-skilled, curious, passionate and visionary team of digital specialists.
