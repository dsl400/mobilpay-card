'use strict';
const crypto = require('crypto');
const rc4 = require( 'arc4' );
const utf8 = require('utf8');

module.exports = {
    encrypt,
    decrypt
};

function encrypt( publicKey, data ) {
    let buff = crypto.randomBytes( 32 );
    let cipher = rc4( 'arc4', buff);
    let encrypted = cipher.encode( data, 'binary', 'base64' );
    let envKey = crypto.publicEncrypt( {
        key: utf8.encode(publicKey),
        padding: crypto.constants.RSA_PKCS1_PADDING
    }, buff );
    return {
        envKey: envKey.toString( 'base64' ),
        envData: encrypted
    };

}

function decrypt( privateKey, envKey, data ) {
    let buffer =  Buffer.from( envKey,'base64' );
    let decrypted = crypto.privateDecrypt({
        key: privateKey,
        padding: crypto.constants.RSA_PKCS1_PADDING
    }, buffer);
    let cipher = rc4('arc4', decrypted);
    return cipher.decode(Buffer.from(data, 'base64'), 'utf8')
}
